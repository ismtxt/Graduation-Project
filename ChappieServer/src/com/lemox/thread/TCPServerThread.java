package com.lemox.thread;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;

import com.lemox.security.RSAGenrator;

public class TCPServerThread extends Thread
{
	public Socket socket;
	public RSAGenrator gen;
	
	public TCPServerThread(){}
	
	public void run ()
	{
		try {
			System.out.print(socket.getInetAddress().toString() + ":");
			System.out.println(socket.getPort());
			gen = new RSAGenrator();
			gen.generate_rsa_key();
			
			System.out.println("Public Key:");
			System.out.println(gen.public_key.getPublicExponent());
			System.out.println(gen.public_key.getModulus());
			
			System.out.println("Private Key:");
			System.out.println(gen.private_key.getPrivateExponent());
			System.out.println(gen.private_key.getModulus());
			
			OutputStream out = socket.getOutputStream();
			out.write(gen.public_key.getModulus().toString().getBytes());
			
			
			InputStream in = socket.getInputStream();
			int len = 0;
			byte[] des_encryed = new byte[128];
			len = in.read(des_encryed);
			
			System.out.println(new String(des_encryed));
			
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.DECRYPT_MODE, gen.private_key);
			byte[] des_key = cipher.doFinal(des_encryed);
			
			System.out.println("DES Key : " + new String(des_key));
			
			byte[] des_encry_source = new byte[48];
			int des_len;
			des_len = in.read(des_encry_source);
			
			System.out.println("DES加密后的数据 : " + new String(des_encry_source));
			
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
}
