package com.lemox.main;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import com.lemox.thread.TCPServerThread;

public class Main {
	public static void main (String[] args) throws Exception
	{
		/*MainThread t = new MainThread();
		t.start();*/
		ServerSocket listen = new ServerSocket(5050);
		System.out.println("Chappie Server is listening...");
		while (true)
		{
			Socket server = listen.accept();
			TCPServerThread t = new TCPServerThread();
			t.socket = server;
			t.start();
		}
		
	}
}

