package com.lemox.security;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public class RSAGenrator 
{
	
	public RSAPublicKey public_key;
	public RSAPrivateKey private_key;
	
	/*
	 * doing nothing.
	 * 
	 * */
	public RSAGenrator (){}
	
	public void generate_rsa_key () throws NoSuchAlgorithmException
	{
		KeyPairGenerator key_pair_generator = KeyPairGenerator.getInstance("RSA");
		key_pair_generator.initialize(1024);
		KeyPair key_pair = key_pair_generator.generateKeyPair();
		
		this.public_key = (RSAPublicKey) key_pair.getPublic();
		this.private_key = (RSAPrivateKey) key_pair.getPrivate();
	}
	
	
	public static void main (String[] args) throws NoSuchAlgorithmException
	{
		RSAGenrator g = new RSAGenrator();
		g.generate_rsa_key();
		System.out.println(g.public_key.getPublicExponent());
		System.out.println(g.public_key.getModulus());
		
		System.out.println("--------------------------------------------------");
		
		System.out.println(g.private_key.getPrivateExponent());
		System.out.println(g.private_key.getModulus());
		
	}
}
