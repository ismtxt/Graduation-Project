package com.lemox.main;

import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.RSAPublicKeySpec;

import javax.crypto.Cipher;

import com.lemox.security.DES;
import com.lemox.security.DESKeyGenerator;

public class Main
{
	public static void main (String[] args) throws Exception
	{
		//Socket s = new Socket(InetAddress.getByName("114.215.144.102"), 5050);
		Socket s = new Socket(InetAddress.getByName("127.0.0.1"), 5050);
		InputStream in = s.getInputStream();
		byte[] b = new byte[1024];
		int len;
		len = in.read(b);
		// ---- len = in.read(b);
		String str = new String(b);
		String public_key_modulus = "";
		for (int i = 0; i < str.length(); i++)
		{
			if (str.charAt(i) >= '0' && str.charAt(i) <= '9')
			{
				public_key_modulus += str.charAt(i);
			}
		}
		System.out.println(str);
		
		// ---- 随机生成DES Key
		byte[] des_key = new byte[8];
		des_key = DESKeyGenerator.generate();
		System.out.println("DES Key : " + new String(des_key));
		
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
		System.out.println(cipher.getProvider().getInfo());
		RSAPublicKeySpec public_key_spec = new RSAPublicKeySpec(new BigInteger(public_key_modulus), new BigInteger("65537"));
		RSAPublicKey public_key = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(public_key_spec);
		cipher.init(Cipher.ENCRYPT_MODE, public_key);
		//byte[] cipher_result = cipher.doFinal("Jiangxi University of Science and Technology".getBytes());
		byte[] cipher_result = cipher.doFinal(des_key);
		System.out.println(new String(cipher_result));
		System.out.println(cipher_result.length);
		
		System.out.println("Sending to server .... ");
		
		OutputStream out = s.getOutputStream();
		out.write(cipher_result);
		System.out.println("Sent to server.");
		
		// ---- 通过DES加密数据发送给服务器
		byte[] des_encry_result;
		des_encry_result = DES.encrypt("Jiangxi University of Science and Technology".getBytes(), des_key);
		System.out.println("DES加密后的数据 : " + new String(des_encry_result));
		System.out.println(des_encry_result.length);
		out.write(des_encry_result);
	}
}
